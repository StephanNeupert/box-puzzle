﻿/*----------------------------------------------------------------
 *                   Box Puzzle Creator
 *         Game idea: Copyright by Andrea Gilbert
 *   All other parts of the source code are licensed under
 *                    CC BY-NC 4.0
 * see: https://creativecommons.org/licenses/by-nc/4.0/legalcode                 
 *----------------------------------------------------------------*/

using System;
using System.Windows.Forms;
using System.Threading;

namespace BoxCreator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(
               (object sender, ThreadExceptionEventArgs t) => Utility.HandleGlobalException(t.Exception));
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(
                (object sender, UnhandledExceptionEventArgs e) => Utility.HandleGlobalException((Exception)e.ExceptionObject));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new BoxCreator());
        }
    }
}
