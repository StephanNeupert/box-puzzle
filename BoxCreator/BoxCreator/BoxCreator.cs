﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BoxCreator
{
    partial class BoxCreator : BufferedForm
    {
        public BoxCreator()
        {
            InitializeComponent();
        }

        private Level level;

        private void butSolve_Click(object sender, EventArgs e)
        {
            string filter = "Box Puzzle level|*.txt";
            string filePath = Utility.SelectFile(filter);
            if (string.IsNullOrEmpty(filePath)) return;

            // Setting lblOutput.Text
            lblOutput.Text = "Working...";
            lblOutput.Update();

            // Start measuring time for execution
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Load Level from file
            level = LevelLoader.LoadFromFile(filePath);
            level.SetSpecialBoxes();

            // Try to solve the level
            LevelSolver solver = new LevelSolver();
            solver.SetLevel(level);
            LevelInfo solutionInfo = solver.SolveLevel(false);

            // Save solution string
            WriteOutput.SaveSolution(solutionInfo);
            
            stopwatch.Stop();

            // Setting lblOutput.Text
            string SolStr = (solutionInfo == null) ? "Unsolvable, " : solutionInfo.MoveBoxList.Count.ToString() + "moves.";
            if (stopwatch.Elapsed.TotalMilliseconds < 5000)
            {
                lblOutput.Text = "Done! " + SolStr + "Elapsed time: " + stopwatch.Elapsed.TotalMilliseconds.ToString() + "ms";
            }
            else if (stopwatch.Elapsed.TotalSeconds < 120)
            {
                lblOutput.Text = "Done! " + SolStr + "Elapsed time: " + stopwatch.Elapsed.TotalSeconds.ToString() + "s";
            }
            else
            {
                lblOutput.Text = "Done! " + SolStr + "Elapsed time: " + ((int)stopwatch.Elapsed.TotalMinutes).ToString() + "min";
            }
           
            
        }

        private void butCreateNew_Click(object sender, EventArgs e)
        {
            CreateNew_Click(false);
        }

        private void butCreateNewRandom_Click(object sender, EventArgs e)
        {
            CreateNew_Click(true);
        }

        private void CreateNew_Click(bool randomStart)
        {
            // Setting lblOutput.Text
            lblOutput.Text = "Working...";
            lblOutput.Update();

            int width = 1;
            int height = 1;
            int numSmall = 1;
            int numLarge = 1;
            int numTries = 100;
            try
            {
                width = int.Parse(txtWidth.Text);
                height = int.Parse(txtHeight.Text);
                numSmall = int.Parse(txtNumSmall.Text);
                numLarge = int.Parse(txtNumLarge.Text);
                numTries = int.Parse(txtNumTries.Text);
            }
            catch
            {
                lblOutput.Text = "Error: Could not read text fields.";
                return;
            }

            if (Math.Max(numSmall, numLarge) + 3 > width * height)
            {
                lblOutput.Text = "Error: Too many boxes.";
                return;
            }
            else if (Math.Min(numSmall, numLarge) < 1)
            {
                lblOutput.Text = "Error: Not enough boxes.";
                return;
            }

            // Start measuring time for execution
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Create LevelCreator
            LevelCreator creator = new LevelCreator(width, height, numLarge, numSmall, numTries);
            Level level = creator.CreatePuzzle(this, lblOutput, randomStart);

            stopwatch.Stop();

            if (level == null)
            {
                lblOutput.Text = "Error: Could not create solvable puzzle.";
                return;
            }

            // Write output level
            WriteOutput.WriteNewLevelFile(level, "NewPuzzle");

            string SolStr = "Done! " + level.MoveBoxList.Count.ToString() + "moves, Elapsed time: ";
            if (stopwatch.Elapsed.TotalMilliseconds < 5000)
            {
                lblOutput.Text = SolStr + stopwatch.Elapsed.TotalMilliseconds.ToString() + "ms";
            }
            else if (stopwatch.Elapsed.TotalSeconds < 120)
            {
                lblOutput.Text = SolStr + stopwatch.Elapsed.TotalSeconds.ToString() + "s";
            }
            else
            {
                lblOutput.Text = SolStr + ((int)stopwatch.Elapsed.TotalMinutes).ToString() + "min";
            }
        }
    }
}
