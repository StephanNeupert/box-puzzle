﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BoxCreator
{
    class WriteOutput
    {
        static public void SaveSolution(LevelInfo solution)
        {
            EnsureFileExists(C.SolutionPath);

            using (TextWriter fileWriter = new StreamWriter(C.SolutionPath, true))
            {
                if (solution == null)
                {
                    fileWriter.WriteLine("// unsolvable");
                }
                else
                {
                    fileWriter.WriteLine("// Box Moves: " + solution.Score.ToString());
                    CreateMoveString(solution.MoveBoxList).ForEach(str => fileWriter.WriteLine("// " + str));
                    fileWriter.WriteLine(" ");
                }

            }
        }

        /// <summary>
        /// Ensures that a given file exists.
        /// </summary>
        /// <param name="filePath"></param>
        static private void EnsureFileExists(string filePath)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
        }

        /// <summary>
        /// Create the string representation for moves and split them after every 20 moves.
        /// </summary>
        /// <param name="moves"></param>
        /// <returns></returns>
        static private List<string> CreateMoveString(List<BoxMove> moves)
        {
            List<string> moveStrings = new List<string>();
            StringBuilder moveString = new StringBuilder();

            // Check for unsolvable levels
            if (moves.Count == 0)
            {
                moveString.Append("unsolvable");
            }

            for (int i = 0; i < moves.Count; i++)
            {
                moveString.Append(moves[i].PosX.ToString());
                moveString.Append(moves[i].PosY.ToString());
                moveString.Append(moves[i].Dir.ToString());
                moveString.Append('-');

                // Add the solution in regular intervals to the SolutionList
                if ((i + 1) % 20 == 0 || (i + 1) == moves.Count)
                {
                    moveStrings.Add(moveString.ToString());
                    moveString.Clear();
                }
                // Add clear lines every 100 moves
                if ((i + 1) % 100 == 0)
                {
                    moveStrings.Add(" ");
                }
            }

            return moveStrings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="fileName"></param>
        static public void WriteNewLevelFile(Level level, string fileName)
        {
            // Do nothing if level is unsolvable
            if (level == null) return;

            string filePath = C.AppPath + fileName + ".txt";
            EnsureFileExists(filePath);
            File.Create(filePath).Close();

            using (TextWriter fileWriter = new StreamWriter(filePath, true))
            {
                // Write global info
                string levelHeader = "// Size  " + level.Width.ToString() + "x" + level.Height.ToString();
                fileWriter.WriteLine(levelHeader);
                fileWriter.WriteLine("// Box Moves  " + level.MoveBoxList.Count.ToString());

                // Write level size
                string levelSize = "SIZ " + level.Width.ToString().PadLeft(2) + " " + level.Height.ToString();
                fileWriter.WriteLine(levelSize);

                // Write start square
                string levelStart = "POS " + level.Pos.X.ToString().PadLeft(2) + " " + level.Pos.Y.ToString().PadLeft(2);
                fileWriter.WriteLine(levelStart);

                // Boxes
                fileWriter.WriteLine("// Boxes: XPos - YPos - Size - Opening - Special(!)");
                for (int i = 0; i < level.BoxList.Count; i++)
                {
                    Box box = level.BoxList[i];

                    StringBuilder boxLine = new StringBuilder("BOX ");
                    boxLine.Append(box.GridPos.X.ToString().PadLeft(2));
                    boxLine.Append(" ");
                    boxLine.Append(box.GridPos.Y.ToString().PadLeft(2));
                    switch (box.Size)
                    {
                        case 0: boxLine.Append(" -"); break;
                        case 1: boxLine.Append(" +"); break;
                    }
                    boxLine.Append(" " + box.Opening.ToString());
                    switch (box.IsSpecial)
                    {
                        case false: boxLine.Append(" ."); break;
                        case true: boxLine.Append(" !"); break;
                    }

                    fileWriter.WriteLine(boxLine.ToString());
                }

                // Solution
                fileWriter.WriteLine("// Solution: ");
                CreateMoveString(level.MoveBoxList).ForEach(str => fileWriter.WriteLine("// " + str));

            }
        }

    }
}
