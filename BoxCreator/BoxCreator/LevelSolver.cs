﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace BoxCreator
{
    class LevelInfo
    {
        public Point Pos;
        public List<Point> BoxPosList;
        public List<BoxMove> MoveBoxList;

        private int fHash;

        public LevelInfo(Level level, List<BoxMove> moveBoxList = null)
        {
            this.Pos = level.Pos.Clone();
            this.BoxPosList = new List<Point>();
            level.BoxList.ForEach(box => this.BoxPosList.Add(box.GridPos.Clone()));
            if (moveBoxList == null)
            {
                this.MoveBoxList = new List<BoxMove>();
            }
            else
            {
                this.MoveBoxList = moveBoxList;
            }

            this.fHash = Pos.X + Pos.Y * level.Width;
            for (int i = 0; i < BoxPosList.Count; i++)
            {
                this.fHash += (BoxPosList[i].X + BoxPosList[i].Y * level.Width) << (i + 1);
            }
        }

        public int GetHash => fHash;
        public int Score => MoveBoxList.Count;
    }

    class LevelInfoComparer : IEqualityComparer<LevelInfo>
    {
        public bool Equals(LevelInfo lev1, LevelInfo lev2)
        {
            if (!lev1.Pos.Equals(lev2.Pos)) return false;

            for (int i = 0; i < lev1.BoxPosList.Count; i++)
            {
                if (! lev1.BoxPosList[i].Equals(lev2.BoxPosList[i])) return false;
            }

            return true;
        }

        public int GetHashCode(LevelInfo lev)
        {
            return lev.GetHash;
        }
    }
    
    /// <summary>
    /// An automatic solver for levels, finding the shortest solution.
    /// </summary>
    class LevelSolver
    {
        public LevelSolver()
        {
            levelComparer = new LevelInfoComparer();
        }

        private Level level;
        private LevelInfo solution;
        private HashSet<LevelInfo> levelHashSet;
        private HashSet<LevelInfo> curLevelHashSet;
        private HashSet<LevelInfo> newLevelHashSet;
        private HashSet<LevelInfo> preLevelHashSet;
        private LevelInfoComparer levelComparer;

        /// <summary>
        /// Initializes the level solver.
        /// </summary>
        /// <param name="level"></param>
        public void SetLevel(Level level)
        {
            this.level = level;
            curLevelHashSet = new HashSet<LevelInfo>(levelComparer);
            levelHashSet = new HashSet<LevelInfo>(levelComparer);
            preLevelHashSet = new HashSet<LevelInfo>(levelComparer);
            // add starting position
            LevelInfo levelInfo = new LevelInfo(this.level);
            levelHashSet.Add(levelInfo);
            curLevelHashSet.Add(levelInfo);
        }

        /// <summary>
        /// Initializes the level solver.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="preLevelHashSet"></param>
        public void SetLevel(Level level, HashSet<LevelInfo> preLevelHashSet)
        {
            this.level = level;
            curLevelHashSet = new HashSet<LevelInfo>(levelComparer);
            levelHashSet = new HashSet<LevelInfo>(levelComparer);
            this.preLevelHashSet = new HashSet<LevelInfo>(preLevelHashSet, levelComparer);
            // add starting position
            LevelInfo levelInfo = new LevelInfo(this.level);
            levelHashSet.Add(levelInfo);
            curLevelHashSet.Add(levelInfo);
        }

        /// <summary>
        /// Solves the given level.
        /// </summary>
        /// <param name="doAbort"></param>
        /// <returns></returns>
        public LevelInfo SolveLevel(bool doAbort = true)
        {
            solution = null;

            // Check if we already computed this level 
            if (preLevelHashSet.Contains(curLevelHashSet.First())) return curLevelHashSet.First();

            // Make moves, until level is solved, or we don't have any new positions
            while (curLevelHashSet.Count > 0 && solution == null)
            {
                newLevelHashSet = new HashSet<LevelInfo>(levelComparer);

                foreach (LevelInfo levelInfo in curLevelHashSet)
                {
                    TryMoves(levelInfo);
                }

                curLevelHashSet = newLevelHashSet;
                levelHashSet.UnionWith(newLevelHashSet);

                if (levelHashSet.Count > 300000 && doAbort)
                {
                    LevelInfo origLevelInfo = levelHashSet.ToList().Find(lvl => lvl.Score == 0);
                    level.LoadInfo(origLevelInfo);
                    level.MoveBoxList = origLevelInfo.MoveBoxList;
                    WriteOutput.WriteNewLevelFile(level, "Bomb" + levelHashSet.Count.ToString());
                    break;
                }
                
            }

            return solution;
        }

        /// <summary>
        /// Try all possible moves and see which can be done.
        /// </summary>
        /// <param name="levelInfo"></param>
        private void TryMoves(LevelInfo levelInfo)
        {
            level.LoadInfo(levelInfo);

            // Get all reachable squares
            HashSet<Point> reachSquares = level.GetReachableSquares();
            reachSquares.IntersectWith(level.BoxList.Select(box => box.GridPos));
         
            // Loop over all reachable squares and directions
            foreach (Point pos in reachSquares)
            {
                foreach(C.DIR dir in C.Dirs)
                {
                    // Load level again
                    level.LoadInfo(levelInfo);
                    level.Pos.X = pos.X;
                    level.Pos.Y = pos.Y;

                    if (level.Move(dir, true, true))
                    {
                        // Movement was successfull
                        List<BoxMove> newMoveBoxList = new List<BoxMove>(levelInfo.MoveBoxList);
                        newMoveBoxList.Add(new BoxMove(pos.X, pos.Y, dir));
                        CheckNewLevel(newMoveBoxList);
                    }
                }
            }
        }

        /// <summary>
        /// Check whether we already know this configuration or if it is clearly unsolvable.
        /// <para> If it is good, add it to the configurations to further look at.</para>
        /// </summary>
        /// <param name="newMoveBoxList"></param>
        private void CheckNewLevel(List<BoxMove> newMoveBoxList)
        {
            // normalize the current position
            List<Point> reachSquares = level.GetReachableSquares().ToList();
            level.Pos = reachSquares.OrderBy(sq => sq.X + sq.Y * level.Width).First();

            LevelInfo newLvlInfo = new LevelInfo(level, newMoveBoxList);

            if (!levelHashSet.Contains(newLvlInfo) && !IsImpossibleConfiguration())
            {
                // Add this LevelInfo to the NewLevelList set
                newLevelHashSet.Add(newLvlInfo);

                // Check whether we solved the level
                if (level.CheckLevelSolved())
                {
                    solution = newLvlInfo;
                }
            }
        }

        /// <summary>
        /// Check for several types of partial configurations that clearly lead to unsolvable problems.
        /// </summary>
        /// <returns></returns>
        private bool IsImpossibleConfiguration()
        {
            Box specSmall = level.SpecBoxSmall;
            Box specLarge = level.SpecBoxLarge;
            if (specSmall.GridPos.Equals(specLarge.GridPos)) return false;

            // Special large box is filled with normal small box
            if (level.BoxList.Exists(box => box.Size == 0 && box.Opening == specLarge.Opening
                                            && box.GridPos.Equals(specLarge.GridPos)))
            {
                return true;
            }
            
            // Special small box is filled with normal large box
            if (level.BoxList.Exists(box => box.Size == 1 && box.Opening == specSmall.Opening
                                            && box.GridPos.Equals(specSmall.GridPos)))
            {
                return true;
            }

            // Special boxes have same opening direction and no large box to move the small to the rank of the large one
            if (specLarge.Opening == specSmall.Opening
                && CheckBoxAtCorrectBorder(specSmall)
                && !CheckSpecBoxSmallMayMoveOut())
            {
                return true;
            }

            // Special box wedged together with another box in corner, such that it is unmovable
            // First if the special boxes are next to the vertex
            Point VertexSquare = specLarge.GridPos.Clone().Move(specLarge.Opening);
            if ((VertexSquare.X == 0 || VertexSquare.X == level.Width - 1)
                && (VertexSquare.Y == 0 || VertexSquare.Y == level.Height - 1)
                && level.BoxList.Exists(box => box.Size == 1 && box.GridPos.Equals(VertexSquare))
                && !level.Pos.Equals(VertexSquare)
                && !level.Pos.Equals(specLarge.GridPos))
            {
                return true;
            }
            VertexSquare = specSmall.GridPos.Clone().Move(specSmall.Opening);
            if ((VertexSquare.X == 0 || VertexSquare.X == level.Width - 1)
                && (VertexSquare.Y == 0 || VertexSquare.Y == level.Height - 1)
                && level.BoxList.Exists(box => box.GridPos.Equals(VertexSquare))
                && !level.Pos.Equals(VertexSquare)
                && !level.Pos.Equals(specSmall.GridPos))
            {
                return true;
            }
            // Not if the special boxes are in the vertices themselves.
            if ((specLarge.GridPos.X == 0 || specLarge.GridPos.X == level.Width - 1)
                && (specLarge.GridPos.Y == 0 || specLarge.GridPos.Y == level.Height - 1))
            {
                int Direction = ((int)specLarge.Opening + 1) % 4;
                if ((Direction == 0 && specLarge.GridPos.Y == 0)
                    || (Direction == 1 && specLarge.GridPos.X == level.Width - 1)
                    || (Direction == 2 && specLarge.GridPos.Y == level.Height - 1)
                    || (Direction == 3 && specLarge.GridPos.X == 0))
                {
                    Direction = (Direction + 2) % 4;
                }
                VertexSquare = specLarge.GridPos.Clone().Move((C.DIR)Direction);
                if (level.BoxList.Exists(box => box.GridPos.Equals(VertexSquare) && (int)box.Opening == (Direction + 2) % 4)
                    && !level.Pos.Equals(VertexSquare))
                {
                    return true;
                }
            }
            if ((specSmall.GridPos.X == 0 || specSmall.GridPos.X == level.Width - 1)
                && (specSmall.GridPos.Y == 0 || specSmall.GridPos.Y == level.Height - 1))
            {
                int Direction = ((int)specSmall.Opening + 1) % 4;
                if ((Direction == 0 && specSmall.GridPos.Y == 0)
                    || (Direction == 1 && specSmall.GridPos.X == level.Width - 1)
                    || (Direction == 2 && specSmall.GridPos.Y == level.Height - 1)
                    || (Direction == 3 && specSmall.GridPos.X == 0))
                {
                    Direction = (Direction + 2) % 4;
                }
                VertexSquare = specSmall.GridPos.Clone().Move((C.DIR)Direction);
                if (level.BoxList.Exists(box => box.GridPos.Equals(VertexSquare) && box.Size == 0 && (int)box.Opening == (Direction + 2) % 4)
                    && !level.Pos.Equals(VertexSquare))
                {
                    return true;
                }
            }
            

            return false;
        }

        /// <summary>
        /// Checks whether the special small box is at the boder opposite of its opening.
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        private bool CheckBoxAtCorrectBorder(Box box)
        {
            switch (box.Opening)
            {
                case C.DIR.N: return box.GridPos.Y == level.Height - 1;
                case C.DIR.E: return box.GridPos.X == 0;
                case C.DIR.S: return box.GridPos.Y == 0;
                case C.DIR.W: return box.GridPos.X == level.Width - 1;
                default: return false;
            }
        }

        /// <summary>
        /// Returns a list of all boxes, that are on the same row/column as the small special box and orthogonal to its opening.
        /// </summary>
        /// <returns></returns>
        private List<Box> GetBoxesInSmallSpecialBorder()
        {
            Box specSmall = level.SpecBoxSmall;
            return level.BoxList.FindAll(box => box.Opening == specSmall.Opening 
                     && (specSmall.Opening.In(C.DIR.N, C.DIR.S) ? box.GridPos.Y == specSmall.GridPos.Y : box.GridPos.X == specSmall.GridPos.X));
        }

        /// <summary>
        /// Check whether a emptyable large box exists to move the small box out of its current row.
        /// </summary>
        /// <returns></returns>
        private bool CheckSpecBoxSmallMayMoveOut()
        {
            Box specSmall = level.SpecBoxSmall;
            return level.BoxList.Exists(box => box.Size == 1 
                                    && !box.Opening.In(specSmall.Opening, specSmall.Opening.Opposite())
                                    && !level.BoxList.Exists(box2 => box2.Size == 0 && box2.Opening == box.Opening && box2.GridPos.Equals(box.GridPos)));
        }
    }
}
