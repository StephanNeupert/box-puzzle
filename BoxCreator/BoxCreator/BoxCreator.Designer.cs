﻿namespace BoxCreator
{
    partial class BoxCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOutput = new System.Windows.Forms.Label();
            this.butSolve = new System.Windows.Forms.Button();
            this.butCreateNew = new System.Windows.Forms.Button();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumSmall = new System.Windows.Forms.TextBox();
            this.lblNumLarge = new System.Windows.Forms.Label();
            this.txtNumLarge = new System.Windows.Forms.TextBox();
            this.lblNumTries = new System.Windows.Forms.Label();
            this.txtNumTries = new System.Windows.Forms.TextBox();
            this.butCreateNewRandom = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOutput
            // 
            this.lblOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(12, 209);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(268, 55);
            this.lblOutput.TabIndex = 0;
            this.lblOutput.Text = "Waiting";
            this.lblOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butSolve
            // 
            this.butSolve.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSolve.Location = new System.Drawing.Point(12, 12);
            this.butSolve.Name = "butSolve";
            this.butSolve.Size = new System.Drawing.Size(89, 43);
            this.butSolve.TabIndex = 1;
            this.butSolve.Text = "Solve";
            this.butSolve.UseVisualStyleBackColor = true;
            this.butSolve.Click += new System.EventHandler(this.butSolve_Click);
            // 
            // butCreateNew
            // 
            this.butCreateNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butCreateNew.Location = new System.Drawing.Point(107, 13);
            this.butCreateNew.Name = "butCreateNew";
            this.butCreateNew.Size = new System.Drawing.Size(89, 42);
            this.butCreateNew.TabIndex = 2;
            this.butCreateNew.Text = "Create New";
            this.butCreateNew.UseVisualStyleBackColor = true;
            this.butCreateNew.Click += new System.EventHandler(this.butCreateNew_Click);
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(107, 76);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(55, 20);
            this.txtWidth.TabIndex = 3;
            this.txtWidth.Text = "3";
            this.txtWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblWidth
            // 
            this.lblWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWidth.Location = new System.Drawing.Point(12, 77);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(54, 20);
            this.lblWidth.TabIndex = 4;
            this.lblWidth.Text = "Width:";
            // 
            // lblHeight
            // 
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(9, 103);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(54, 20);
            this.lblHeight.TabIndex = 6;
            this.lblHeight.Text = "Height:";
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(107, 102);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(55, 20);
            this.txtHeight.TabIndex = 5;
            this.txtHeight.Text = "3";
            this.txtHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "#SmallBox:";
            // 
            // txtNumSmall
            // 
            this.txtNumSmall.Location = new System.Drawing.Point(107, 155);
            this.txtNumSmall.Name = "txtNumSmall";
            this.txtNumSmall.Size = new System.Drawing.Size(55, 20);
            this.txtNumSmall.TabIndex = 9;
            this.txtNumSmall.Text = "2";
            this.txtNumSmall.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNumLarge
            // 
            this.lblNumLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumLarge.Location = new System.Drawing.Point(12, 129);
            this.lblNumLarge.Name = "lblNumLarge";
            this.lblNumLarge.Size = new System.Drawing.Size(89, 20);
            this.lblNumLarge.TabIndex = 8;
            this.lblNumLarge.Text = "#LargeBox:";
            // 
            // txtNumLarge
            // 
            this.txtNumLarge.Location = new System.Drawing.Point(107, 128);
            this.txtNumLarge.Name = "txtNumLarge";
            this.txtNumLarge.Size = new System.Drawing.Size(55, 20);
            this.txtNumLarge.TabIndex = 7;
            this.txtNumLarge.Text = "2";
            this.txtNumLarge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNumTries
            // 
            this.lblNumTries.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumTries.Location = new System.Drawing.Point(12, 182);
            this.lblNumTries.Name = "lblNumTries";
            this.lblNumTries.Size = new System.Drawing.Size(74, 20);
            this.lblNumTries.TabIndex = 12;
            this.lblNumTries.Text = "#Tries:";
            // 
            // txtNumTries
            // 
            this.txtNumTries.Location = new System.Drawing.Point(107, 181);
            this.txtNumTries.Name = "txtNumTries";
            this.txtNumTries.Size = new System.Drawing.Size(55, 20);
            this.txtNumTries.TabIndex = 11;
            this.txtNumTries.Text = "100";
            this.txtNumTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // butCreateNewRandom
            // 
            this.butCreateNewRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butCreateNewRandom.Location = new System.Drawing.Point(202, 13);
            this.butCreateNewRandom.Name = "butCreateNewRandom";
            this.butCreateNewRandom.Size = new System.Drawing.Size(89, 42);
            this.butCreateNewRandom.TabIndex = 13;
            this.butCreateNewRandom.Text = "Create New Random";
            this.butCreateNewRandom.UseVisualStyleBackColor = true;
            this.butCreateNewRandom.Click += new System.EventHandler(this.butCreateNewRandom_Click);
            // 
            // BoxCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 273);
            this.Controls.Add(this.butCreateNewRandom);
            this.Controls.Add(this.lblNumTries);
            this.Controls.Add(this.txtNumTries);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNumSmall);
            this.Controls.Add(this.lblNumLarge);
            this.Controls.Add(this.txtNumLarge);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.butCreateNew);
            this.Controls.Add(this.butSolve);
            this.Controls.Add(this.lblOutput);
            this.Name = "BoxCreator";
            this.Text = "Box Puzzle Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Button butSolve;
        private System.Windows.Forms.Button butCreateNew;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumSmall;
        private System.Windows.Forms.Label lblNumLarge;
        private System.Windows.Forms.TextBox txtNumLarge;
        private System.Windows.Forms.Label lblNumTries;
        private System.Windows.Forms.TextBox txtNumTries;
        private System.Windows.Forms.Button butCreateNewRandom;
    }
}

