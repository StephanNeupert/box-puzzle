﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace BoxCreator
{
    class InverseMove
    {
        public InverseMove(int x, int y, C.DIR dir, bool onlysmall)
        {
            this.PosX = x;
            this.PosY = y;
            this.Dir = dir;
            this.OnlySmall = onlysmall;
        }

        public int PosX;
        public int PosY;
        public C.DIR Dir;
        public bool OnlySmall;
    }

    /// <summary>
    /// This class creates a level with predetermined size and number of boxes.
    /// </summary>
    class LevelCreator
    {
        public LevelCreator(int width, int height, int numBoxLarge, int numBoxSmall, int tries)
        {
            rnd = new Random();

            this.width = width;
            this.height = height;
            this.numBoxLarge = numBoxLarge;
            this.numBoxSmall = numBoxSmall;
            this.tries = tries;
        }

        private Random rnd;
        int width;
        int height;
        int numBoxLarge;
        int numBoxSmall;

        int tries;

        Level curLevel;
        HashSet<LevelInfo> preLevelInfos;

        /// <summary>
        /// Creates new puzzles and returns the best one.
        /// </summary>
        /// <param name="form"></param>
        /// <param name="lblOutput"></param>
        /// <param name="randomStart"></param>
        /// <returns></returns>
        public Level CreatePuzzle(BoxCreator form, Label lblOutput, bool randomStart)
        {
            Level bestLevel = null;

            for (int i = 0; i < tries; i++)
            {
                // Create new level
                LevelInfo levelInfo = CreateNew(randomStart);
                // Check whether this is better than the previous one
                if (levelInfo != null && (bestLevel == null || levelInfo.Score > bestLevel.MoveBoxList.Count))
                {
                    bestLevel = CreateLevelFromLevelInfo(levelInfo);
                    // Just to be able to stop midway, we write intermediate best results...
                    if (levelInfo.Score > 20) WriteOutput.WriteNewLevelFile(bestLevel, "NewPuzzle");
                }

                // Update progress
                if (randomStart || i % 10 == 9)
                {
                    lblOutput.Text = "Working... (" + (i + 1).ToString() + " of " + tries.ToString() + " tests done)";
                    Application.DoEvents();
                }
            }

            return bestLevel;
        }

        /// <summary>
        /// Saves the levelInfo in a proper level with all the data.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <returns></returns>
        private Level CreateLevelFromLevelInfo(LevelInfo levelInfo)
        {
            curLevel.LoadInfo(levelInfo);
            curLevel.MoveBoxList = levelInfo.MoveBoxList;
            return curLevel;
        }

        /// <summary>
        /// Creates a single new optimized puzzle.
        /// </summary>
        /// <param name="randomStart"></param>
        /// <returns></returns>
        private LevelInfo CreateNew(bool randomStart)
        {
            LevelInfo bestLevelInfo = null;
            preLevelInfos = new HashSet<LevelInfo>();

            if (randomStart)
            {
                // Initialize a random start position
                InitializeRandom();
                // Check for solvablility at all start positions of boxes
                LevelInfo curLevelInfo = TestInitial();

                if (curLevelInfo.Score < 2)
                {
                    bestLevelInfo = null;
                }
                else
                {
                    bestLevelInfo = curLevelInfo;
                    // Make 10 tries to modify this intelligently
                    for (int i = 0; i < 10; i++)
                    {
                        curLevel.LoadInfo(curLevelInfo);
                        LevelInfo newLevelInfo = ModifyBackwards();
                        if (newLevelInfo.Score > bestLevelInfo.Score)
                        {
                            bestLevelInfo = newLevelInfo;
                        }
                    }
                }
            }
            else
            {
                // Initialize the level at the final position
                InitializeAtEnd();
                // Modify Intelligently
                bestLevelInfo = ModifyBackwards();
            }
            return bestLevelInfo;
        }

        /// <summary>
        /// Test curLevel for solvability.
        /// </summary>
        /// <returns></returns>
        private LevelInfo TestInitial()
        {
            LevelInfo curLevelInfo = new LevelInfo(curLevel);
            LevelInfo firstSolution = null;

            // Get a jumbled list of starting positions
            List<Point> startPosList = curLevel.StartBoxList.ConvertAll(box => box.GridPos);
            startPosList.Jumble(rnd);

            int index = -1;
            while (++index < curLevelInfo.BoxPosList.Count && firstSolution == null)
            {
                // Set start position correctly
                curLevel.LoadInfo(curLevelInfo);
                curLevel.StartPos = startPosList[index].Clone();
                curLevel.Pos = curLevel.StartPos.Clone();
                // Test level
                LevelSolver solver = new LevelSolver();
                solver.SetLevel(curLevel);
                firstSolution = solver.SolveLevel();
            }

            if (firstSolution != null)
            {
                curLevelInfo.MoveBoxList = firstSolution.MoveBoxList;
                curLevelInfo.Pos = curLevel.StartPos.Clone();

                AddSolutionToPreLevelHashSet(curLevelInfo);
            }

            return curLevelInfo;
        }

        /// <summary>
        /// Optimize curLevel, by going backwards.
        /// </summary>
        /// <returns></returns>
        private LevelInfo ModifyBackwards()
        {
            // Save first version with TestResults
            LevelInfo curlevelInfo = new LevelInfo(curLevel);
            LevelSolver solver = new LevelSolver();
            solver.SetLevel(curLevel);
            curlevelInfo.MoveBoxList = solver.SolveLevel().MoveBoxList;

            // Modify the level, by going one box movement back per step
            bool DoMoreMoves = true;
            while (DoMoreMoves)
            {
                DoMoreMoves = false;

                // Get List of all possible moves
                curLevel.LoadInfo(curlevelInfo);
                List<InverseMove> possibleMoves = GetPossibleMoves();
                possibleMoves.Jumble(rnd);

                int i = -1;
                while (++i < possibleMoves.Count && !DoMoreMoves)
                {
                    // Apply backwards move
                    ApplyInvMove(curlevelInfo, possibleMoves[i]);

                    // Test level
                    solver.SetLevel(curLevel, preLevelInfos); //= new LevelSolver(CurLevel);
                    LevelInfo newSolution = solver.SolveLevel();

                    if (newSolution != null)
                    {
                        // reset boxes to their original position
                        ApplyInvMove(curlevelInfo, possibleMoves[i]);
                        LevelInfo newSolutionInfo = new LevelInfo(curLevel, newSolution.MoveBoxList);

                        AddSolutionToPreLevelHashSet(newSolutionInfo);

                        // Check, if this was an improvement
                        if (newSolution.Score > curlevelInfo.Score)
                        {
                            curlevelInfo = newSolutionInfo;
                            DoMoreMoves = true;
                        }
                    }
                }
            }

            return curlevelInfo;
        }

        /// <summary>
        /// Applies an inverse move to modify the level.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="move"></param>
        private void ApplyInvMove(LevelInfo levelInfo, InverseMove move)
        {
            curLevel.LoadInfo(levelInfo);
            // Find new position
            Point oldPos = new Point(move.PosX, move.PosY);
            Point newPos = oldPos.Clone();
            newPos = newPos.Move(move.Dir);
            curLevel.StartPos = newPos.Clone();
            // Move boxes
            Box smallBox = curLevel.BoxList.Find(box => box.Size == 0 && box.GridPos.Equals(oldPos));
            Box largeBox = curLevel.BoxList.Find(box => box.Size == 1 && box.GridPos.Equals(oldPos));

            if (move.OnlySmall)
            {
                // We already checked previously that we can move the small box (which exists)
                smallBox.GridPos = newPos.Clone();
            }
            else
            {
                // We already checked previously that we can move the large box (which exists)
                largeBox.GridPos = newPos.Clone();
                // Test whether we should move the small box
                if (smallBox != null && !(smallBox.Opening == move.Dir && largeBox.Opening == move.Dir.Opposite()))
                {
                    smallBox.GridPos = newPos.Clone();
                }
            }

            // Copy starting position to current position
            curLevel.Pos = curLevel.StartPos.Clone();
            curLevel.StartBoxList.Clear();
            curLevel.BoxList.ForEach(box => curLevel.StartBoxList.Add(box.Clone()));
            // Delete MoveList
            curLevel.MoveList.Clear();
        }

        /// <summary>
        /// Get a starting position with only move to do.
        /// </summary>
        private void InitializeAtEnd()
        {
            curLevel = new Level();
            curLevel.Width = width;
            curLevel.Height = height;

            // Set start position and final position next to each other
            curLevel.StartPos = new Point(rnd.Next(width), rnd.Next(height));
            Point finalPos;
            C.DIR firstDir;
            do
            {
                finalPos = curLevel.StartPos.Clone();
                firstDir = (C.DIR)rnd.Next(4);
                finalPos = finalPos.Move(firstDir);
            } while (!finalPos.Within(width, height));

            // Set special boxes
            // Large box at FinalPos with opening to StartPos
            Box specBoxLarge = new Box(finalPos, 1, firstDir.Opposite(), true);
            curLevel.StartBoxList.Add(specBoxLarge);
            // Small box at StartPos with opening not to StartPos
            Box specBoxSmall = new Box(curLevel.StartPos.Clone(), 0, (C.DIR)(((int)firstDir + 1 + rnd.Next(3)) % 4), true);
            curLevel.StartBoxList.Add(specBoxSmall);

            // Set normal boxes
            SetNormalBoxes(specBoxLarge, specBoxSmall);

            // Copy starting position to current position
            curLevel.Pos = curLevel.StartPos.Clone();
            curLevel.StartBoxList.ForEach(box => curLevel.BoxList.Add(box.Clone()));
            curLevel.SetSpecialBoxes();
        }

        /// <summary>
        /// Initialize the level at a random constellation.
        /// </summary>
        private void InitializeRandom()
        {
            curLevel = new Level();
            curLevel.Width = width;
            curLevel.Height = height;

            // Set arbitrary start position
            curLevel.StartPos = new Point(rnd.Next(width), rnd.Next(height));

            Point specPos;
            Box specBoxSmall;
            Box specBoxLarge;
            // Set Special small box
            do
            {
                specPos = new Point(rnd.Next(width), rnd.Next(height));
                specBoxSmall = new Box(specPos.Clone(), 0, (C.DIR)rnd.Next(4), true);
                specPos = specPos.Move(specBoxSmall.Opening);
            } while (!specPos.Within(width, height));
            curLevel.StartBoxList.Add(specBoxSmall);
            // Set Special large box
            do
            {
                specPos = new Point(rnd.Next(width), rnd.Next(height));
                specBoxLarge = new Box(specPos.Clone(), 1, (C.DIR)(((int)specBoxSmall.Opening + 3 + rnd.Next(3)) % 4), true);
                specPos = specPos.Move(specBoxLarge.Opening);
            } while (!specPos.Within(width, height)
                     || specBoxSmall.GridPos.Equals(specBoxLarge.GridPos));
            curLevel.StartBoxList.Add(specBoxLarge);

            // Add other boxes
            SetNormalBoxes(specBoxLarge, specBoxSmall);

            // Copy starting position to current position
            curLevel.Pos = curLevel.StartPos.Clone();
            curLevel.StartBoxList.ForEach(box => curLevel.BoxList.Add(box.Clone()));
            curLevel.SetSpecialBoxes();
        }

        /// <summary>
        /// Adds the normal boxes to curLevel at a random position.
        /// </summary>
        /// <param name="specBoxLarge"></param>
        /// <param name="specBoxSmall"></param>
        private void SetNormalBoxes(Box specBoxLarge, Box specBoxSmall)
        {
            Point newSquare;
            Box newBox;
            for (int i = 1; i < numBoxSmall + numBoxLarge - 1; i++)
            {
                do
                {
                    newSquare = new Point(rnd.Next(width), rnd.Next(height));
                    newBox = new Box(newSquare.Clone(), (i < numBoxSmall) ? 0 : 1, (C.DIR)rnd.Next(4), false);
                    newSquare = newSquare.Move(newBox.Opening);
                } while (curLevel.StartBoxList.Exists(box => box.GridPos.Equals(newBox.GridPos))
                         || !newSquare.Within(width, height)
                         || curLevel.StartBoxList.Exists(box => box.GridPos.Equals(newSquare) && box.Opening == newBox.Opening.Opposite()));

                curLevel.StartBoxList.Add(newBox);
            }
        }

        /// <summary>
        /// Creates a list of all possible inverse modifications that can be done to the level.
        /// </summary>
        /// <returns></returns>
        private List<InverseMove> GetPossibleMoves()
        {
            List<InverseMove> possibleMoves = new List<InverseMove>();

            // Get all reachable squares
            HashSet<Point> reachSquares = curLevel.GetReachableSquares();
            reachSquares.IntersectWith(curLevel.BoxList.ConvertAll(box => box.GridPos));

            foreach (Point square in reachSquares)
            {
                Box boxSmall = curLevel.BoxList.Find(box => box.GridPos.Equals(square) && box.Size == 0);
                Box boxLarge = curLevel.BoxList.Find(box => box.GridPos.Equals(square) && box.Size == 1);

                // First try to move only the small box
                if (boxSmall != null)
                {
                    foreach (C.DIR dir in C.Dirs) // Direction from previous position to current one
                    {
                        if (CheckForSmallMove(boxSmall, boxLarge, dir))
                        {
                            possibleMoves.Add(new InverseMove(square.X, square.Y, dir.Opposite(), true));
                        }
                    }
                }

                // Now try to move the large box as well
                if (boxLarge != null)
                {
                    foreach (C.DIR dir in C.Dirs)
                    {
                        if (CheckForLargeMove(boxSmall, boxLarge, dir))
                        {
                            possibleMoves.Add(new InverseMove(square.X, square.Y, dir.Opposite(), false));
                        }
                    }
                }
            }

            return possibleMoves;
        }

        /// <summary>
        /// Checks whether one can do an inverse move with the small box.
        /// </summary>
        /// <param name="boxSmall"></param>
        /// <param name="boxLarge"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        private bool CheckForSmallMove(Box boxSmall, Box boxLarge, C.DIR dir)
        {
            Point square = boxSmall.GridPos;
            Point target = square.Clone().Move(dir.Opposite());

            return target.Within(width, height)
                && boxSmall.Opening != dir
                && (boxLarge == null || boxLarge.Opening == dir.Opposite())
                && !curLevel.BoxList.Exists(box => box.GridPos.Equals(target) && box.Size == 0)
                && !curLevel.BoxList.Exists(box => box.GridPos.Equals(target) && box.Size == 1 && box.Opening != dir);
        }

        /// <summary>
        /// Checks whether one can do an inverse move with the large box.
        /// </summary>
        /// <param name="boxSmall"></param>
        /// <param name="boxLarge"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        private bool CheckForLargeMove(Box boxSmall, Box boxLarge, C.DIR dir)
        {
            Point square = boxLarge.GridPos;
            Point target = square.Clone().Move(dir.Opposite());

            return target.Within(width, height)
                && boxLarge.Opening != dir
                && (boxSmall == null || !curLevel.BoxList.Exists(box => box.GridPos.Equals(target) && box.Size == 0))
                && (boxLarge.Opening == dir.Opposite() || !curLevel.BoxList.Exists(box => box.GridPos.Equals(target) && box.Size == 0 && box.Opening != dir))
                && !curLevel.BoxList.Exists(box => box.GridPos.Equals(target) && box.Size == 1);
        }

        /// <summary>
        /// Adds a solution (that contains the correct moves) to the preLevelInfos.
        /// </summary>
        /// <param name="solution"></param>
        private void AddSolutionToPreLevelHashSet(LevelInfo solution)
        {
            curLevel.LoadInfo(solution);
            List<BoxMove> moves = new List<BoxMove>(solution.MoveBoxList);

            for (int i = 0; i < solution.MoveBoxList.Count; i++)
            {
                // normalize the current position
                HashSet<Point> reachSquares = curLevel.GetReachableSquares();
                curLevel.Pos = reachSquares.OrderBy(sq => sq.X + sq.Y * curLevel.Width).First();
                
                preLevelInfos.Add(new LevelInfo(curLevel, moves));

                // Make next move.
                curLevel.Pos.X = moves[0].PosX;
                curLevel.Pos.Y = moves[0].PosY;
                curLevel.Move(moves[0].Dir);

                moves.RemoveAt(0);
            }
        }



    }
}
