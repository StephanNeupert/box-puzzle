﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace BoxCreator
{
    class C // for Constants
    {
        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public static string AppPath => Application.StartupPath + DirSep;
        public static string SolutionPath => AppPath + "AutoSolutions.txt";

        public enum DIR { N, E, S, W }
        public static readonly DIR[] Dirs = new DIR[] { DIR.N, DIR.E, DIR.S, DIR.W };
    }

    class BufferedForm : Form
    {
        public BufferedForm()
        {
            this.DoubleBuffered = true;
            this.ResizeRedraw = true;
        }
    }

    static class Utility
    {
        /// <summary>
        /// Checks if an object is contained in an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        public static bool Equals(this Point p1, Point p2)
        {
            return p2 != null && p1.X == p2.X && p1.Y == p2.Y;
        }

        public static Point Clone(this Point p)
        {
            return new Point(p.X, p.Y);
        }

        public static Point Move(this Point p, C.DIR dir)
        {
            switch (dir)
            {
                case C.DIR.N: p.Y--; break;
                case C.DIR.E: p.X++; break;
                case C.DIR.S: p.Y++; break;
                case C.DIR.W: p.X--; break;
            }
            return p;
        }

        public static bool Within(this Point p, int width, int height)
        {
            return (p.X >= 0 && p.X < width && p.Y >= 0 && p.Y < height);
        }

        public static void Jumble<T>(this List<T> list, Random rnd)
        {
            for (int i = list.Count; i > 0; i--)
            {
                int index = rnd.Next(i);
                list.Add(list[index]);
                list.RemoveAt(index);
            }
        }

        public static C.DIR Opposite(this C.DIR dir)
        {
            switch (dir)
            {
                case C.DIR.N: return C.DIR.S;
                case C.DIR.E: return C.DIR.W;
                case C.DIR.S: return C.DIR.N;
                case C.DIR.W: return C.DIR.E;
                default: return C.DIR.S;
            }
        }

        /// <summary>
        /// Opens the file browser and returns the selected files.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="startFolder"></param>
        /// <returns></returns>
        public static string SelectFile(string filter, string startFolder = null)
        {
            var openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = (startFolder != null) ? startFolder : C.AppPath;
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = filter;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.CheckFileExists = true;

            string filePath = string.Empty;

            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error while showing the file browser." + C.NewLine + Ex.Message, "File browser error");
            }
            finally
            {
                openFileDialog?.Dispose();
            }

            return filePath;
        }

        /// <summary>
        /// Handles a global unexpected exception and displays a warning message to the user.
        /// </summary>
        /// <param name="Ex"></param>
        public static void HandleGlobalException(Exception Ex)
        {
            try
            {
                LogException(Ex);
                string errorString = "An error occured: " + Ex.Message + C.NewLine + "Try to continue playing? Selecting 'no' will quit the game.";
                var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) Application.Exit();
            }
            catch
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Logs an exception message to AppPath/ErrorLog.txt.
        /// </summary>
        /// <param name="ex"></param>
        public static void LogException(Exception ex)
        {
            string errorPath = C.AppPath + "ErrorLog.txt";
            using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
            {
                textFile.WriteLine(ex.ToString());
            }
        }
    }
}
