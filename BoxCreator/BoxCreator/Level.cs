﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;

namespace BoxCreator
{
    /// <summary>
    /// A box in the level.
    /// </summary>
    class Box
    {
        public Point GridPos;
        public int Size; // 0 = small, 1 = big
        public C.DIR Opening;
        public bool IsSpecial;

        public Box()
        {
            this.GridPos = new Point(0, 0);
        }

        public Box(Point pos, int size, C.DIR opening, bool isSpec)
        {
            this.GridPos = pos;
            this.Size = size;
            this.Opening = opening;
            this.IsSpecial = isSpec;
        }

        public Box(Box copyBox)
        {
            this.GridPos = copyBox.GridPos.Clone();
            this.IsSpecial = copyBox.IsSpecial;
            this.Opening = copyBox.Opening;
            this.Size = copyBox.Size;
        }

        public bool Equals(Box box2)
        {
            return this.Size == box2.Size
                && this.Opening == box2.Opening
                && this.GridPos.Equals(box2.GridPos)
                && this.IsSpecial == box2.IsSpecial;
        }

        public Box Clone()
        {
            return new Box(this);
        }
    }

    /// <summary>
    /// The move of a box
    /// </summary>
    class BoxMove
    {
        public BoxMove(int x, int y, C.DIR dir)
        {
            this.PosX = x;
            this.PosY = y;
            this.Dir = dir;
        }

        public int PosX;
        public int PosY;
        public C.DIR Dir;

        public BoxMove Clone()
        {
            return new BoxMove(PosX, PosY, Dir);
        }
    }

    /// <summary>
    /// A whole level.
    /// </summary>
    class Level
    {
        public Level()
        {
            StartBoxList = new List<Box>();
            BoxList = new List<Box>();
            MoveList = new List<C.DIR>();
        }

        public int Width;
        public int Height;

        public Point StartPos;
        public Point Pos;
        public List<Box> StartBoxList;
        public List<Box> BoxList;
        public List<C.DIR> MoveList;
        public List<BoxMove> MoveBoxList; // just for passing this info to the file writer!

        public Box SpecBoxLarge { get; private set; }
        public Box SpecBoxSmall { get; private set; }

        /// <summary>
        /// Loads a level from a given LevelInfo data.
        /// <para> Warning: This only updates the positions, not sizes or openings of boxes!</para>
        /// </summary>
        /// <param name="levelInfo"></param>
        public void LoadInfo(LevelInfo levelInfo)
        {
            Debug.Assert(BoxList.Count == levelInfo.BoxPosList.Count, "LevelInfo writes to wrong level");
            // Moreover this assumes, that the order of the boxes NEVER changes!

            Pos.X = levelInfo.Pos.X;
            Pos.Y = levelInfo.Pos.Y;

            for (int i = 0; i < BoxList.Count; i++)
            {
                BoxList[i].GridPos.X = levelInfo.BoxPosList[i].X;
                BoxList[i].GridPos.Y = levelInfo.BoxPosList[i].Y;
            }
        }

        /// <summary>
        /// Sets references to special boxes correctly.
        /// </summary>
        public void SetSpecialBoxes()
        {
            SpecBoxSmall = BoxList.Find(box => box.IsSpecial && box.Size == 0);
            SpecBoxLarge = BoxList.Find(box => box.IsSpecial && box.Size == 1);
        }

        /// <summary>
        /// Moves the box(es) at the current location in a given direction.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="doSaveMove"></param>
        /// <param name="forceMoveBox"></param>
        /// <returns></returns>
        public bool Move(C.DIR dir, bool doSaveMove = true, bool forceMoveBox = false)
        {
            // Get boxes at the same position as the player
            Box boxSmall = BoxList.Find(box => box.GridPos.Equals(Pos) && box.Size == 0);
            Box boxLarge = BoxList.Find(box => box.GridPos.Equals(Pos) && box.Size == 1);

            // Set boxes back to null, if we wouldn't move them
            if (boxLarge != null && boxLarge.Opening == dir) boxLarge = null;
            if (boxSmall != null && boxSmall.Opening == dir 
                  && (boxLarge == null || boxLarge.Opening == dir.Opposite())) boxSmall = null;

            // Abort movement if we force box movements, but don't do move any
            if (forceMoveBox && boxLarge == null && boxSmall == null) return false;

            bool CanMove = CheckLevelBoundaries(dir)
                        && !CheckLevelSolved()
                        && CheckBoxes(dir, boxLarge, boxSmall);

            if (CanMove)
            {               
                // Move player
                Pos = Pos.Move(dir);

                // Move boxes
                if (boxLarge != null)
                {
                    boxLarge.GridPos.X = Pos.X;
                    boxLarge.GridPos.Y = Pos.Y;
                }
                if (boxSmall != null)
                {
                    boxSmall.GridPos.X = Pos.X;
                    boxSmall.GridPos.Y = Pos.Y;
                }

                // Remember this move
                if (!doSaveMove) MoveList.Add(dir);
            }

            return CanMove;
        }

        /// <summary>
        /// Check whether we have solved the level.
        /// </summary>
        /// <returns></returns>
        public bool CheckLevelSolved()
        {
            return SpecBoxSmall.GridPos.Equals(SpecBoxLarge.GridPos);
        }

        /// <summary>
        /// Checks whether the level boundaries prevent moving.
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        private bool CheckLevelBoundaries(C.DIR dir)
        {
            bool CanMove = true;

            switch (dir)
            {
                case C.DIR.N: if (Pos.Y == 0) CanMove = false; break;
                case C.DIR.E: if (Pos.X == Width - 1) CanMove = false; break;
                case C.DIR.S: if (Pos.Y == Height - 1) CanMove = false; break;
                case C.DIR.W: if (Pos.X == 0) CanMove = false; break;
            }

            return CanMove;
        }

        /// <summary>
        /// Check whether other boxes prevent moving the player.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="boxLarge"></param>
        /// <param name="boxSmall"></param>
        /// <returns></returns>
        private bool CheckBoxes(C.DIR dir, Box boxLarge, Box boxSmall)
        {
            Point newPos = Pos.Clone();
            newPos = newPos.Move(dir);

            Box stopBoxSmall = BoxList.Find(box => box.GridPos.Equals(newPos) && box.Size == 0);
            Box stopBoxLarge = BoxList.Find(box => box.GridPos.Equals(newPos) && box.Size == 1);

            if (boxLarge != null)
            {
                return (stopBoxSmall == null && stopBoxLarge == null);
            }
            else if (boxSmall != null)
            {
                return (stopBoxSmall == null 
                    && (stopBoxLarge == null || stopBoxLarge.Opening == dir.Opposite()));
            }
            else // no boxes to move
            {
                return ((stopBoxSmall == null || stopBoxSmall.Opening == dir.Opposite()) 
                     && (stopBoxLarge == null || stopBoxLarge.Opening == dir.Opposite()));
            }
        }

        /// <summary>
        /// Create a the set of squares that one can reach from the current position.
        /// </summary>
        /// <returns></returns>
        public HashSet<Point> GetReachableSquares()
        {
            HashSet<Point> reachPos = new HashSet<Point>() { Pos.Clone() };
            HashSet<Point> newReachPos = new HashSet<Point>();
            HashSet<Point> checkPos = new HashSet<Point>() { Pos };

            while (checkPos.Count > 0)
            {
                foreach (Point square in checkPos)
                {
                    foreach (C.DIR dir in C.Dirs)
                    {
                        // Check whether there is a box at current position
                        if (!BoxList.Exists(box => box.GridPos.Equals(square) && box.Opening != dir))
                        {
                            Point newSquare = square.Clone();
                            newSquare = newSquare.Move(dir);

                            if (newSquare.Within(Width, Height)
                                && !BoxList.Exists(box => box.GridPos.Equals(newSquare) && box.Opening != dir.Opposite()))
                            {
                                newReachPos.Add(newSquare);
                            }
                        }
                    }
                }
                // Get newly reached squares
                checkPos.UnionWith(newReachPos);
                checkPos.ExceptWith(reachPos);
                // And add them to the global reachable positions
                reachPos.UnionWith(checkPos);
                newReachPos.Clear();
            }

            return reachPos;
        }

    }
}
