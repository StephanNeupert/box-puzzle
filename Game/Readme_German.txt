|------------------------|
|                  _     |
|   |_| BoxPuzzle | |    |
|                        |
|------------------------|

1) Ziel des Spiels:
Bringe die kleine rote Box in die gro�e blaue Box.

2) Objekte:
- Einige Boxen, dargestellt durch drei Seiten eines Quadrates.
- Die Boxen kommen in zwei verschiedenen Gr��en.
- Eine kleine rote Box, eine gro�e blaue Box und eine verschiedene Anzahl an schwarzen Boxen.
- Ein kleiner schwarzer Punkt - der Spieler.

3) Regeln:
- Du kannst dich zu benachbarten Feldern bewegen, aber nicht diagonal.
- Du kannst dich von der offenen Seite her in eine Box bewegen.
- Falls du dich in einer Box befinest, kannst du diese verschieben, falls:
  a) Die Box an der Seite eine Wand hat, in der du sie verschieben willst.
  b) Am Zielfeld keine weitere Box steht oder du eine kleine Box in eine gro�e Box schiebst.
- Analoge Regeln gelten beim Verschieben von zwei ineinander geschachtelten Boxen.
- Boxen k�nnen niemals von au�en verschoben werden.
- Boxen k�nnen auch nicht gezogen werden.
- Liegen zwei Boxen nebeneinander, k�nnen diese nicht beide gleichzeitig verschoben werden, 
  d.h. eine Box kann eine andere nicht weiterschieben.

4) Buttons und Tasten
- Button "Restart Level": Startet den Level neu.
- Button "Next Puzzle": Geht zum n�chsten Level.
- Button "Previous Puzzle": Geht zum vorherigen Level.
- Button "Undo Move": Macht den letzten Zug r�ckg�ngig.

Tasten:
- Pfeiltasten: Bewegt den Spieler.
- "W": Bewegt den Spieler nach oben.
- "A": Bewegt den Spieler nach links.
- "S": Bewegt den Spieler nach unten.
- "D": Bewegt den Spieler nach rechts.
- Leertaste: Startet den Level neu.
- "U": Macht den letzten Zug r�ckg�ngig.

5) Neue Levels kreieren
Die Levels sind in Dateien ".txt" gespeichert, die mit "Puzzle" starten.
Diese Dateien m�ssen sich im gleichen Verzeichnis wie die BoxMaze.exe Datei befinden.
Zeilen in der Textdatei:
 "Siz a b": Gr��e des Spielfelds, "a" is die Breite, "b" ist die H�he
 "Pos a b": Startposition des Spielers, "a" ist die X-Koordinate, "b" ist die Y-Koordinate (mit 0 beginnend)
 "Box a b c d e": Definition einer Box:
     "a" ist die X-Koordinate (mit 0 beginnend)
     "b" ist die Y-Koordinate (mit 0 beginnend)
     "c" ist die Gr��e der Box: "+" f�r gro� und "-" f�r klein
     "d" ist die �ffnung der Box als Himmelsrichtung: "N", "E", "S", "W"
     "e" sagt aus, ob dies spezielle Box ist: "!" definiert eine rote/blaue Box und "." eine schwarze normale Box

6) Gespeicherte L�sungen
Das Spiel speichert L�sungen in der Datei "Solutions.txt".
Jede Zeile speichert dabei eine L�sung.
Nach der Nummer des Level kommt die Zugfolge als Liste von Himmelsrichtungen, in die gegangen wurde.


Viel Spa�!

