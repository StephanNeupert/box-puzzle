// MEDIUM-HARD
// Size  4x3
// Box Moves  37
SIZ 4 3
POS 3 0
// Boxes: XPos - YPos - Size - Opening - Special(!)
BOX 3 0 - W !
BOX 1 0 + W !
BOX 2 0 - S .
BOX 0 1 - S .
BOX 2 2 + E .
BOX 3 0 + N .
// Solution: 
// 30S-31W-21W-11S-12N-11E-21E-12W-01N-02E-31S-22N-21W-11W-32N-12E-01S-00E-10S-11W-
// 01N-11S-02N-20W-31N-22E-01S-12E-30S-22N-21N-20E-31S-32W-22N-21N-20E-
