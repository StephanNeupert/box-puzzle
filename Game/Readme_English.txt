    |------------------------|
    |                  _     |
    |   |_| BoxPuzzle | |    |
    |                        |
    |------------------------|

1) Goal
Move the small red box into the big blue one.

2) Game objects
- A playing grid, usually 3x4 squares.
- Several boxes on a grid, displayed as a square with one open side:
  - One small red box
  - One big blue box
  - And several black boxes, that can be either small or big
- A small black dot - the player.

3) Rules
- You can move horizontally or vertically to neighboring squares, but not diagonally.
- You can move into a box, if you approach it from its open side.
- If you are within a box, you can move it, assuming:
  - The box has a solid side in the direction you want to move. Otherwise you will just leave the box.
  - On the target square is no other box, or you are within a small box and move into a big one.
- Similar rules apply if you are within two boxes, one contained in the other.
- Boxes can never be moved by pushing or pulling from the outside.
- If two boxes are placed next to each other, they cannot be moved at the same time by pushing one against the other.

4) Buttons and hotkeys
- Button "Restart Level": Returns the level to its initial setup.
- Button "Next Puzzle": Aborts the current level and starts the next level.
- Button "Previous Puzzle": Aborts the current level and starts the previous level.
- Button "Undo Move": Undoes the latest move. Can be pressed multiple time to go back several moves.

Keys:
- Arrow keys: Moves the player.
- "W": Moves the player up.
- "A": Moves the player left.
- "S": Moves the player down.
- "D": Moves the player right.
- Space: Restarts the level.
- "U": Undoes the latest move.

5) Create new levels
The level data are contained in the text files ".txt" starting with "Puzzle".
These files have to be contained in the same directory as the game "BoxPuzzle.exe".

The level data files, contain the following lines, in an arbitrary order:
 "Siz": Size of the play grid. The numbers following it signify:
    - 1st number: Grid width
    - 2nd number: Grid height
 "Pos": Player position at the beginning of the level. The numbers following it signify:
    - 1st number: X-coordinate (zero-indexed)
    - 2nd number: Y-coordinate (zero-indexed) 
 "Box": Definition of a box. The following characters signify:
    - 1st number: X-coordinate (zero-indexed)
    - 2nd number: Y-coordinate (zero-indexed) 
    - 3rd char.: Size of the box. "+" for a big box and "-" for a small box
    - 4th char.: Open side of the box. One of "N", "E", "S", "W" 
    - 5th char.: Marks whether it is a special box: "!" for a red or blue box and "." for a normal black one.


6) Saved solutions
The game saves your solutions in the file "solutions.txt". Each row marks one solution.
Each row starts with the number of the level. After that all movements are displayed via the 
respective cardinal direction.


Have fun!

