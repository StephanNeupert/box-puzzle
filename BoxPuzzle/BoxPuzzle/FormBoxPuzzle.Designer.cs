﻿namespace BoxPuzzle
{
    partial class FormBoxPuzzle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PicBoxLevel = new System.Windows.Forms.PictureBox();
            this.lblLevelTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // PicBoxLevel
            // 
            this.PicBoxLevel.BackColor = System.Drawing.SystemColors.Control;
            this.PicBoxLevel.InitialImage = null;
            this.PicBoxLevel.Location = new System.Drawing.Point(10, 36);
            this.PicBoxLevel.Name = "PicBoxLevel";
            this.PicBoxLevel.Size = new System.Drawing.Size(284, 284);
            this.PicBoxLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicBoxLevel.TabIndex = 2;
            this.PicBoxLevel.TabStop = false;
            // 
            // lblLevelTitle
            // 
            this.lblLevelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevelTitle.Location = new System.Drawing.Point(10, 9);
            this.lblLevelTitle.Name = "lblLevelTitle";
            this.lblLevelTitle.Size = new System.Drawing.Size(284, 24);
            this.lblLevelTitle.TabIndex = 4;
            this.lblLevelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormBoxPuzzle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 394);
            this.Controls.Add(this.lblLevelTitle);
            this.Controls.Add(this.PicBoxLevel);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormBoxPuzzle";
            this.Text = "Box Puzzle";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PicBoxLevel;
        private System.Windows.Forms.Label lblLevelTitle;
    }
}

