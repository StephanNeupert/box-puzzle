﻿using System;
using System.Windows.Forms;

namespace BoxPuzzle
{
    class MyButton : Button
    {
        public MyButton(int xPos, int yPos, int width, int height, string text)
        {
            Left = xPos;
            Width = width;
            Top = yPos;
            Height = height;
            Text = text;
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Space:
                    return true;
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                case Keys.Shift | Keys.Space:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Space)
            {
                base.OnKeyDown(e);
            }
        }
    }
}
