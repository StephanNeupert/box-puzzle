﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace BoxPuzzle
{
    /// <summary>
    /// A box in the level.
    /// </summary>
    class Box
    {
        public Point Pos;
        public int Size; // 0 = small, 1 = big
        public C.DIR Opening;
        public bool IsSpecial;

        public Box() { }

        public Box Clone()
        {
            Box newBox = new Box();
            newBox.Pos = Pos.Clone();
            newBox.IsSpecial = IsSpecial;
            newBox.Opening = Opening;
            newBox.Size = Size;
            return newBox;
        }
    }

    /// <summary>
    /// A wall in the level.
    /// </summary>
    class Wall
    {
        public Wall() { }

        public Wall(Point pos)
        {
            Pos = pos;
        }

        public Point Pos;

        public Wall Clone()
        {
            return new Wall(Pos.Clone());
        }
    }

    /// <summary>
    /// A move made by the player
    /// </summary>
    class Move
    {
        public Move(C.DIR dir, Box largeBox, Box smallBox)
        {
            Direction = dir;
            LargeBox = largeBox;
            SmallBox = smallBox;
        }

        public C.DIR Direction { get; private set; }
        public Box LargeBox { get; private set; }
        public Box SmallBox { get; private set; }

        public bool Equals(Move otherMove)
        {
            return Direction == otherMove?.Direction
                && SmallBox == otherMove?.SmallBox
                && LargeBox == otherMove?.LargeBox;
        }
    }

    /// <summary>
    /// A full level.
    /// </summary>
    class Level
    {
        public Level() { }

        public int Width;
        public int Height;

        public Point PlayerPos;

        public List<Box> BoxList;
        public List<Wall> WallList;
        public List<Move> MoveList;
        public int NumBoxMoves;

        public int GridSize => 2 * (140 / Math.Max(Width, Height));

        public Level Clone()
        {
            Level newLevel = new Level();
            newLevel.Width = Width;
            newLevel.Height = Height;
            newLevel.PlayerPos = PlayerPos.Clone();
            newLevel.BoxList = new List<Box>(BoxList.ConvertAll(box => box.Clone()));
            newLevel.WallList = WallList; // shallow copy, as walls may not change
            newLevel.MoveList = new List<Move>(MoveList);
            newLevel.NumBoxMoves = NumBoxMoves;

            return newLevel;
        }

        /// <summary>
        /// Tries to move a player in a given direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="isReplaying"></param>
        /// <returns></returns>
        public bool MovePlayer(C.DIR direction, bool isReplaying = false)
        {
            // Get boxes at the same position as the player
            Box smallBox = BoxList.Find(box => box.Pos.Equals(PlayerPos) && box.Size == 0);
            Box largeBox = BoxList.Find(box => box.Pos.Equals(PlayerPos) && box.Size == 1);

            // Set boxes back to null, if we wouldn't move them
            if (largeBox != null && largeBox.Opening == direction) largeBox = null;
            if (smallBox != null && smallBox.Opening == direction
                && (largeBox == null || largeBox.Opening == direction.Opposite())) smallBox = null;
         
            bool doMove = isReplaying || CanMove(direction, largeBox, smallBox);

            if (doMove)
            { 
                PlayerPos = PlayerPos.Move(direction);
                
                if (largeBox != null)
                {
                    largeBox.Pos = PlayerPos.Clone();
                }
                if (smallBox != null)
                {
                    smallBox.Pos = PlayerPos.Clone();
                }

                // Remember this move
                Move newMove = new Move(direction, smallBox, largeBox);
                if (IsLastMoveReversed(newMove))
                {
                    MoveList.RemoveAt(MoveList.Count - 1);
                }
                else
                {
                    MoveList.Add(newMove);
                }

                if (smallBox != null || largeBox != null) NumBoxMoves++; // always do this, even when replaying!
            }

            return doMove;
        }

        /// <summary>
        /// Returns whether the player may move in the given direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="largeBox"></param>
        /// <param name="smallBox"></param>
        /// <returns></returns>
        private bool CanMove(C.DIR direction, Box largeBox, Box smallBox)
        {
            return CheckLevelBoundaries(direction)
                && !IsLevelSolved()
                && CheckBoxes(direction, largeBox, smallBox); 
        }

        /// <summary>
        /// Check whether the level is solved.
        /// </summary>
        /// <returns></returns>
        public bool IsLevelSolved()
        {
            Box smallBox = BoxList.Find(box => box.IsSpecial && box.Size == 0);
            Box largeBox = BoxList.Find(box => box.IsSpecial && box.Size == 1);

            try
            {
                return smallBox.Pos.Equals(largeBox.Pos);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the level boundary prevents moving in the direction
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private bool CheckLevelBoundaries(C.DIR direction)
        {
            switch (direction)
            {
                case C.DIR.N: return PlayerPos.Y != 0;
                case C.DIR.E: return PlayerPos.X != Width - 1;
                case C.DIR.S: return PlayerPos.Y != Height - 1;
                case C.DIR.W: return PlayerPos.X != 0;
                default: return false;
            }
        }

        /// <summary>
        /// Checks whether the given boxes may move in the desired direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="largeBox"></param>
        /// <param name="smallBox"></param>
        /// <returns></returns>
        private bool CheckBoxes(C.DIR direction, Box largeBox, Box smallBox)
        {
            Point NewPos = PlayerPos.Clone().Move(direction);

            // Check as well for walls
            if (WallList.Exists(wal => wal.Pos.Equals(NewPos))) return false;

            Box stopBoxSmall = BoxList.Find(box => box.Pos.Equals(NewPos) && box.Size == 0);
            Box stopBoxLarge = BoxList.Find(box => box.Pos.Equals(NewPos) && box.Size == 1);

            if (largeBox != null)
            {
                return (stopBoxSmall == null && stopBoxLarge == null);
            }
            else if (smallBox != null)
            {
                return (stopBoxSmall == null && (stopBoxLarge == null || stopBoxLarge.Opening == direction.Opposite()));
            }
            else // no boxes to move
            {
                return ((stopBoxSmall == null || stopBoxSmall.Opening == direction.Opposite()) 
                     && (stopBoxLarge == null || stopBoxLarge.Opening == direction.Opposite()));
            }
        }

        /// <summary>
        /// Checks whether the current direction to move inverts the previous move
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private bool IsLastMoveReversed(Move newMove)
        {
            if (MoveList.Count == 0) return false;

            Move lastMove = MoveList[MoveList.Count - 1];
            return newMove.Direction == lastMove.Direction.Opposite()
                && !((newMove.SmallBox == null) ^ (lastMove.SmallBox == null))
                && !((newMove.LargeBox == null) ^ (lastMove.LargeBox == null));
        }

        /// <summary>
        /// Replays all the moves in the given moveList.
        /// </summary>
        /// <param name="moveList"></param>
        public void ReplayMoves(List<Move> moveList)
        {
            NumBoxMoves = 0;
            moveList.ForEach(move => MovePlayer(move.Direction, true));
        }

    }
}
