﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace BoxPuzzle
{
    static class Utility
    {
        public static C.DIR Opposite(this C.DIR direction)
        {
            switch (direction)
            {
                case C.DIR.N: return C.DIR.S;
                case C.DIR.E: return C.DIR.W;
                case C.DIR.S: return C.DIR.N;
                case C.DIR.W: return C.DIR.E;
                default: return C.DIR.N;
            }
        }

        public static Point Move(this Point square, C.DIR direction)
        {
            switch (direction)
            {
                case C.DIR.N: square.Y--; break;
                case C.DIR.E: square.X++; break;
                case C.DIR.S: square.Y++; break;
                case C.DIR.W: square.X--; break;
            }
            return square;
        }

        public static Point Clone(this Point point)
        {
            return new Point(point.X, point.Y);
        }

        public static bool Equals(this Point p1, Point p2)
        {
            if (p2 == null) return false;
            else return p1.X == p2.X && p1.Y == p2.Y;
        }

        /// <summary>
        /// Handles a global unexpected exception and displays a warning message to the user.
        /// </summary>
        /// <param name="Ex"></param>
        public static void HandleGlobalException(Exception Ex)
        {
            try
            {
                LogException(Ex);
                string errorString = "An error occured: " + Ex.Message + C.NewLine + "Try to continue playing? Selecting 'no' will quit the game.";
                var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) Application.Exit();
            }
            catch
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Logs an exception message to AppPath/ErrorLog.txt.
        /// </summary>
        /// <param name="ex"></param>
        public static void LogException(Exception ex)
        {
            string errorPath = C.AppPath + "ErrorLog.txt";
            using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
            {
                textFile.WriteLine(ex.ToString());
            }
        }

    }

    class C // for Contstants
    {
        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public static string AppPath => Application.StartupPath + DirSep;
        public static string SolutionPath => AppPath + "Solutions.txt";

        public enum DIR { N, E, S, W }
    }
}
