﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace BoxPuzzle
{    
    partial class FormBoxPuzzle : Form
    {
        public FormBoxPuzzle()
        {
            InitializeComponent();
            CreateComponents();

            CreatePuzzleNameList();
            levelIndex = 0;
            LoadLevel(0);
            renderer.RenderLevel();
        }

        private List<string> puzzleNames;

        private int levelIndex;

        private Level startLevel;
        private Level curLevel;
        private Renderer renderer;

        private void CreateComponents()
        {
            var button = new MyButton(15, 340, 60, 40, "Restart Level");
            button.Click += new EventHandler(Button_RestartLevel_Click);
            Controls.Add(button);

            button = new MyButton(85, 340, 60, 40, "Previous Level");
            button.Click += new EventHandler(Button_PrevLevel_Click);
            Controls.Add(button);

            button = new MyButton(155, 340, 60, 40, "Next Level");
            button.Click += new EventHandler(Button_NextLevel_Click);
            Controls.Add(button);

            button = new MyButton(225, 340, 60, 40, "Undo Move");
            button.Click += new EventHandler(Button_UndoMove_Click);
            Controls.Add(button);
        }

        /// <summary>
        /// Load the sorted list of all levels.
        /// </summary>
        private void CreatePuzzleNameList()
        {
            puzzleNames = Directory.GetFiles(C.AppPath, "*.txt", SearchOption.TopDirectoryOnly)
                                   .Select(file => Path.GetFileName(file))
                                   .ToList()
                                   .FindAll(file => file.StartsWith("Puzzle"));
            puzzleNames.Sort();
        }

        /// <summary>
        /// Loads a new level.
        /// </summary>
        /// <param name="index"></param>
        private void LoadLevel(int index)
        {
            // Load Level from file
            string FilePath = C.AppPath + puzzleNames[index];
            startLevel = LevelLoader.LoadFromFile(FilePath);
            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel, this.PicBoxLevel);

            UpdateLevelTitle();
        }

        /// <summary>
        /// Updates the level title according to whether it was already solved or not.
        /// </summary>
        private void UpdateLevelTitle()
        {
            string levelTitle = "Level " + (levelIndex + 1).ToString();
            int numMoves = Solutions.FindMinBoxMoves(puzzleNames[levelIndex]);
            if (numMoves != -1)
            {
                levelTitle += "  (solved in " + numMoves.ToString() + " box moves)";
            }
            else
            {
                levelTitle += "  (unsolved)";
            }
            lblLevelTitle.Text = levelTitle;
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        { 
            switch (e.KeyCode)
            {
                case Keys.Up: 
                case Keys.W:
                    MakeMove(C.DIR.N); break;
                case Keys.Right: 
                case Keys.D:    
                    MakeMove(C.DIR.E); break;
                case Keys.Down: 
                case Keys.S:
                    MakeMove(C.DIR.S); break;
                case Keys.Left: 
                case Keys.A:    
                    MakeMove(C.DIR.W); break;
                case Keys.U:
                    Button_UndoMove_Click(null, null); break;
                case Keys.Space:
                    Button_RestartLevel_Click(null, null); break;
                case Keys.Enter:
                    if (curLevel.IsLevelSolved()) Button_NextLevel_Click(null, null); break;
            }
        
        }

        /// <summary>
        /// Moves the player in one direction.
        /// </summary>
        /// <param name="direction"></param>
        private void MakeMove(C.DIR direction)
        {
            // Try to move player in desired direction
            bool levelChanged = curLevel.MovePlayer(direction);

            if (levelChanged)
            { 
                renderer.RenderLevel();
            }

            if (curLevel.IsLevelSolved())
            {
                lblLevelTitle.Text = "Level " + (levelIndex + 1).ToString() + "  (solved in " + curLevel.NumBoxMoves.ToString() + " box moves)";
                Solutions.SaveSolution(curLevel.MoveList.ConvertAll(move => move.Direction), 
                                       curLevel.NumBoxMoves, puzzleNames[levelIndex]);
            }
        }

        /// <summary>
        /// Presents the next level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_NextLevel_Click(object sender, EventArgs e)
        {
            levelIndex = (levelIndex + 1) % puzzleNames.Count;
            LoadLevel(levelIndex);
            renderer.RenderLevel();
        }

        /// <summary>
        /// Presents the previous level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_PrevLevel_Click(object sender, EventArgs e)
        {
            levelIndex = (levelIndex - 1 + puzzleNames.Count) % puzzleNames.Count;
            LoadLevel(levelIndex);
            renderer.RenderLevel();
        }

        /// <summary>
        /// Restarts the level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_RestartLevel_Click(object sender, EventArgs e)
        {
            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel, PicBoxLevel);
            renderer.RenderLevel();
        }

        /// <summary>
        /// Goes one move back.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_UndoMove_Click(object sender, EventArgs e)
        {
            List<Move> moveList = curLevel.MoveList;
            if (moveList == null || moveList.Count == 0) return;
            moveList.RemoveAt(moveList.Count - 1);

            curLevel = startLevel.Clone();
            curLevel.ReplayMoves(moveList);

            renderer = new Renderer(curLevel, PicBoxLevel);
            renderer.RenderLevel();
        }
    }





}
