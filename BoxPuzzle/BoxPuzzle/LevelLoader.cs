﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace BoxPuzzle
{
    static class LevelLoader
    {
        /// <summary>
        /// Loads a new level from a file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Level LoadFromFile(string filePath)
        {
            Level level = new Level();
            level.BoxList = new List<Box>();
            level.WallList = new List<Wall>();
            level.MoveList = new List<Move>();
            level.NumBoxMoves = 0;
            level.PlayerPos = new System.Drawing.Point();

            // Read in all lines
            List<string> FileStrings = ReadFile(filePath);
            FileStrings.ForEach(str => EvaluateLine(level, str));

            SanitizeInput(level);

            return level;
        }

        /// <summary>
        /// Reads in all files of the level.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static List<string> ReadFile(string filePath)
        {
            List<string> fileLines = new List<string>();

            try
            {
                // Open the text file using a stream reader.
                using (StreamReader fileReader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            fileLines.Add(line);
                        }
                    }
                }
            }
            catch
            {
                // Load default empty level
                fileLines.Add("Siz 3 3");
                fileLines.Add("Pos 1 1");
            }

            return fileLines;
        }

        /// <summary>
        /// Reads the level info from a single line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="line"></param>
        private static void EvaluateLine(Level level, string line)
        {
            try
            {
                if (line.Length < 3) return;

                switch (line.Substring(0, 3).ToUpper())
                {
                    case "SIZ": ReadSizeLine(level, SplitLine(line)); break;
                    case "POS": ReadPlayerPosLine(level, SplitLine(line)); break;
                    case "BOX": ReadBoxLine(level, SplitLine(line)); break;
                    case "WAL": ReadWallLine(level, SplitLine(line)); break;
                }
            }
            catch
            {
                // silent fail!
            }
        }

        /// <summary>
        /// Separates the various info components of a line.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private static List<string> SplitLine(string line)
        {
            List<string> infos = line.Split(' ').ToList();
            infos.RemoveAll(str => string.IsNullOrWhiteSpace(str));
            return infos;
        }

        /// <summary>
        /// Reads the size of the level from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadSizeLine(Level level, List<string> infos)
        {
            level.Width = int.Parse(infos[1]);
            level.Height = int.Parse(infos[2]);            
        }

        /// <summary>
        /// Reads the start position for the player from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadPlayerPosLine(Level level, List<string> infos)
        {
            level.PlayerPos.X = int.Parse(infos[1]);
            level.PlayerPos.Y = int.Parse(infos[2]);
        }

        /// <summary>
        /// Reads the info for a new box from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadBoxLine(Level level, List<string> infos)
        {
            Box box = new Box();
            box.Pos = new System.Drawing.Point(int.Parse(infos[1]), int.Parse(infos[2]));

            switch (infos[3])
            {
                case "+": box.Size = 1; break;
                case "-": box.Size = 0; break;
                default: box.Size = 0; break;
            }

            switch (infos[4])
            {
                case "N": box.Opening = C.DIR.N; break;
                case "E": box.Opening = C.DIR.E; break;
                case "S": box.Opening = C.DIR.S; break;
                case "W": box.Opening = C.DIR.W; break;
                default: box.Opening = C.DIR.N; break;
            }

            box.IsSpecial = infos.Count > 5 && infos[5].Equals("!");

            level.BoxList.Add(box);
        }

        /// <summary>
        /// Reads the position of a new wall from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadWallLine(Level level, List<string> infos)
        {
            var pos = new System.Drawing.Point(int.Parse(infos[1]), int.Parse(infos[2]));
            level.WallList.Add(new Wall(pos));
        }

        /// <summary>
        /// Ensure that no invalid inputs are in the level data.
        /// </summary>
        /// <param name="level"></param>
        private static void SanitizeInput(Level level)
        {
            // Level size
            if (level.Width < 1) level.Width = 1;
            else if (level.Width > 10) level.Width = 10;

            if (level.Height < 1) level.Height = 1;
            else if (level.Height > 10) level.Height = 10;

            //Starting position
            if (level.PlayerPos.X < 0) level.PlayerPos.X = 0;
            else if (level.PlayerPos.X >= level.Width) level.PlayerPos.X = level.Width - 1;

            if (level.PlayerPos.Y < 0) level.PlayerPos.Y = 0;
            else if (level.PlayerPos.Y >= level.Height) level.PlayerPos.Y = level.Height - 1;

            // Box placements
            level.BoxList.RemoveAll(box => box.Pos.X < 0 || box.Pos.X >= level.Width
                                        || box.Pos.Y < 0 || box.Pos.Y >= level.Height);

            // Wall placements
            level.WallList.RemoveAll(wal => wal.Pos.X < 0 || wal.Pos.X >= level.Width
                                         || wal.Pos.Y < 0 || wal.Pos.Y >= level.Height);
        }


    }
}
