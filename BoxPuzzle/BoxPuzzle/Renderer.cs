﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace BoxPuzzle
{
    class Renderer
    {
        /// <summary>
        /// A rendeded to produce the graphical output.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="picBox"></param>
        public Renderer(Level level, PictureBox picBox)
        {
            this.level = level;
            this.picBox = picBox;
        }

        private Level level;
        private PictureBox picBox;

        private int gridSize => level.GridSize;

        /// <summary>
        /// Displays the currently level in the specified picutre-box.
        /// </summary>
        public void RenderLevel()
        {
            int picWidth = level.Width * gridSize + 2;
            int picHeight = level.Height * gridSize + 2;
            Bitmap image = new Bitmap(picWidth, picHeight);
            DrawRect(image, 0, 0, image.Width, image.Height, Color.White);

            DrawGrid(image);
            DrawPlayer(image, level.PlayerPos);

            level.BoxList.ForEach(box => DrawBox(image, box));
            level.WallList.ForEach(wal => DrawWall(image, wal));

            picBox.Image = image;
        }

        /// <summary>
        /// Translates grid coordinates into image coordinates.
        /// </summary>
        /// <param name="gridCoord"></param>
        /// <returns></returns>
        private int GetImageCoordiates(int gridCoord)
        {
            return 2 + gridCoord * level.GridSize + level.GridSize / 2;
        }

        /// <summary>
        /// Draws the basic square grid.
        /// </summary>
        /// <param name="image"></param>
        private void DrawGrid(Bitmap image)
        {
            // Draw border rectangle
            DrawLine(image, 0, 0, image.Width, 3, false);
            DrawLine(image, 0, image.Height - 3, image.Width, 3, false);
            DrawLine(image, 0, 0, image.Height, 3, true);
            DrawLine(image, image.Width - 3, 0, image.Height, 3, true);

            // Draw lines within the rectangle
            for (int X = 1; X < level.Width; X++)
            {
                DrawLine(image, 1 + X * gridSize, 3, image.Height - 6, 2, true, Color.Gray);
            }
            for (int Y = 1; Y < level.Height; Y++)
            {
                DrawLine(image, 3, 1 + Y * gridSize, image.Width - 6, 2, false, Color.Gray);
            }
        }

        /// <summary>
        /// Draws the player dot.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="gridPos"></param>
        private void DrawPlayer(Bitmap image, Point gridPos)
        {
            int posX = GetImageCoordiates(gridPos.X);
            int posY = GetImageCoordiates(gridPos.Y);
            int radius = (3 * gridSize) / 28;
            DrawCircle(image, posX, posY, radius, Color.Black);
        }

        /// <summary>
        /// Draws a box.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="box"></param>
        private void DrawBox(Bitmap image, Box box)
        {
            int posX = GetImageCoordiates(box.Pos.X);
            int posY = GetImageCoordiates(box.Pos.Y);
            int length = 2 * (((8 + 4 * box.Size) * gridSize) / 28);
            int width = (3 * gridSize) / 28;
            Color color = Color.Black;
            if (box.IsSpecial)
            {
                if (box.Size == 0) color = Color.Crimson;
                else color = Color.Blue;
            }
            // Draw all the boundaries
            if (box.Opening != C.DIR.N)
            {
                DrawLine(image, posX - length / 2, posY - length / 2, length, width, false, color);
            }
            if (box.Opening != C.DIR.E)
            {
                DrawLine(image, posX + length / 2 - width, posY - length / 2, length, width, true, color);
            }
            if (box.Opening != C.DIR.S)
            {
                DrawLine(image, posX - length / 2, posY + length / 2 - width, length, width, false, color);
            }
            if (box.Opening != C.DIR.W)
            {
                DrawLine(image, posX - length / 2, posY - length / 2, length, width, true, color);
            }
        }

        /// <summary>
        /// Draws a wall.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="wall"></param>
        private void DrawWall(Bitmap image, Wall wall)
        {
            int posX = 2 + wall.Pos.X * gridSize;
            int posY = 2 + wall.Pos.Y * gridSize;
            DrawRect(image, posX, posY, gridSize, gridSize, Color.Black);
        }


        private void DrawLine(Bitmap image, int startX, int startY, int length, int width, bool isVertical, Color color)
        {
            if (isVertical)
            {
                DrawRect(image, startX, startY, width, length, color);
            }
            else
            {
                DrawRect(image, startX, startY, length, width, color);
            }
        }

        private void DrawLine(Bitmap image, int startX, int startY, int length, int width, bool isVertical)
        {
            DrawLine(image, startX, startY, length, width, isVertical, Color.Black);
        }

        private void DrawRect(Bitmap image, int startX, int startY, int width, int height, Color color)
        {
            using (Graphics g = Graphics.FromImage(image))
            {
                using (Brush b = new SolidBrush(color))
                {
                    g.FillRectangle(b, startX, startY, width, height);
                }
                g.Dispose();
            }
        }

        private void DrawCircle(Bitmap image, int centerX, int centerY, int radius, Color color)
        {
            using (Graphics g = Graphics.FromImage(image))
            {
                using (Brush b = new SolidBrush(color))
                {
                    g.FillEllipse(b, centerX - radius, centerY - radius, 2 * radius, 2 * radius);
                }
            }
        }


    }
}
