﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BoxPuzzle
{
    static class Solutions
    {
        /// <summary>
        /// Saves a solution in "Solutions.txt".
        /// </summary>
        /// <param name="moveList"></param>
        /// <param name="levelIndex"></param>
        static public void SaveSolution(List<C.DIR> moveList, int numBoxMoves, string puzzleName)
        {
            EnsureFileExists();
            string solution = CreateSolutionString(moveList, numBoxMoves, puzzleName).TrimEnd();

            if (!CheckSolutionExists(solution))
            {
                using (TextWriter fileWriter = new StreamWriter(C.SolutionPath, true))
                {
                    fileWriter.WriteLine(solution);
                }
            }
        }

        /// <summary>
        /// Creates a new solution file, if none yet exists.
        /// </summary>
        static private void EnsureFileExists()
        {
            if (!File.Exists(C.SolutionPath))
            {
                File.Create(C.SolutionPath).Close();
            }
        }

        /// <summary>
        /// Creates the string representation of the solution
        /// </summary>
        /// <param name="moveList"></param>
        /// <param name="levelIndex"></param>
        /// <returns></returns>
        static private string CreateSolutionString(List<C.DIR> moveList, int numBoxMoves, string puzzleName)
        {
            return puzzleName + " (" + numBoxMoves.ToString() + "): "
                    + string.Join(string.Empty, moveList.ConvertAll(move => move.ToString()));
        }

        /// <summary>
        /// Checks whether a given solution already exists in the solution file.
        /// </summary>
        /// <param name="solution"></param>
        /// <returns></returns>
        static private bool CheckSolutionExists(string solution)
        {
            try
            {
                // Open the text file using a stream reader.
                using (StreamReader fileReader = new StreamReader(C.SolutionPath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (line.TrimEnd().Equals(solution)) return true;
                    }
                }
            }
            catch
            {
                // can't do anything
            }
            return false;
        }

        /// <summary>
        /// Returns a list of all solutions to a given puzzle.
        /// </summary>
        /// <param name="puzzleName"></param>
        /// <returns></returns>
        static private List<string> FindSolutionStrings(string puzzleName)
        {
            List<string> solutionStrings = new List<string>();

            if (!File.Exists(C.SolutionPath)) return solutionStrings;

            try
            {
                using (StreamReader Stream = new StreamReader(C.SolutionPath))
                {
                    string line;
                    while ((line = Stream.ReadLine()) != null)
                    {
                        if (line.StartsWith(puzzleName)) solutionStrings.Add(line);
                    }
                }
            }
            catch
            {
                // can't do anything
            }

            return solutionStrings;
        }

        /// <summary>
        /// It returns the number of box moves of the best solution.
        /// <para> Returns "-1" if the level is still unsolved. </para>
        /// </summary>
        /// <param name="puzzleName"></param>
        /// <returns></returns>
        static public int FindMinBoxMoves(string puzzleName)
        {
            List<string> solutionsStrings = FindSolutionStrings(puzzleName);
            if (solutionsStrings.Count == 0) return -1;

            int numMoves = int.MaxValue;
            foreach (string solution in solutionsStrings)
            {
                try
                {
                    string moves = solution.Split('(', ')')[1];
                    numMoves = Math.Min(numMoves, int.Parse(moves));
                }
                catch
                {
                    // do nothing with this solution
                }
            }

            return numMoves;
        }

        /// <summary>
        /// It returns the number of moves of the best solution.
        /// <para> Returns "-1" if the level is still unsolved. </para>
        /// /// </summary>
        /// <param name="levelIndex"></param>
        /// <returns></returns>
        static public int FindMinPlayerMoves(string puzzleName)
        {
            List<string> solutionsStrings = FindSolutionStrings(puzzleName);
            if (solutionsStrings.Count == 0) return -1;

            int numMoves = int.MaxValue;
            foreach (string solution in solutionsStrings)
            {
                try
                {
                    string moves = solution.Split(':')[1];
                    numMoves = Math.Min(numMoves, CountSteps(moves));
                }
                catch
                {
                    // do nothing with this solution
                }
            }

            return numMoves;
        }

        /// <summary>
        /// Counts to number of steps in a solution line
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        static private int CountSteps(string line)
        {
            List<char> CharList = new List<char> { 'N', 'E', 'S', 'W' };
            return line.ToList().Count(c => CharList.Contains(c));
        }

    }
}
