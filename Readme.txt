Welcome to BoxPuzzle!

This is a surpisingly hard puzzle game, originally invented by Andrea Gilbert.

This repository contains:
- The actual game "BoxPuzzle.sln".
- An automatic solver and puzzle creator "BoxCreator.sln".
- Over 40 new levels in the folder "Game".

To compile, just build the solution files with any C# compiler that supports C# 6.0.
No external libraries or similar are required.

Copyright:
- Game design: (c) Andrea Gilber, all rights reserved.
- Everything else: (c) Stephan Neupert, CC BY-NC 4.0 (2017)